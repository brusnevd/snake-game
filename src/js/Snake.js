class Snake {
	constructor() {
		this.body = [];
	    this.body[0] = createVector(floor(w/2), floor(h/2));
	    this.xdir = 0;
	    this.ydir = 0;
	    this.len  = 1;
	}

	setDir(x, y) {
		this.xdir = x;
		this.ydir = y;
	}


	eat(footPos) {
		let x = this.body[this.body.length-1].x;
		let y = this.body[this.body.length-1].y;
		if (x == footPos.x && y == footPos.y) {
			this.grow();
			return true;
		}
		return false;
	}

	death() {
		let x = this.body[this.body.length-1].x;
		let y = this.body[this.body.length-1].y;

		if (x > w-1 || x < 0 || y > h-1 || y < 0) {
			return true;
		}
		
		for (let i = 0; i < this.body.length-1; i++) {
			if (this.body[i].x == x && this.body[i].y == y) {
				return true;
			}
		}
		return false;
	}

	grow() {
		let head = this.body[this.body.length-1].copy();
		this.len++;
		this.body.push(head);
	}

	update() {
		let head = this.body[this.body.length-1].copy();
	    this.body.shift();
	    head.x += this.xdir;
	    head.y += this.ydir;
	    this.body.push(head);

	}

	show() {
		for (let i = 0; i < this.body.length; i++) {
			fill(255);
			noStroke();
			rect(this.body[i].x, this.body[i].y, 1, 1)
		}
	}
}