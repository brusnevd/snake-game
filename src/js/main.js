let snake;
let rez = 20;//resolution
let food;
let w, h;//width, height


function setup() {
	let cnv = createCanvas(800, 550);
	cnv.class('gamecanva');
    w = floor(width/rez);
    h = floor(height/rez);
	frameRate(5);
    snake = new Snake();
	foodLocation();
}

function draw() {
    scale(rez);
	background(2,229,202);

	if (snake.eat(food)) {
		foodLocation();
	}
	snake.update();
	snake.show();

	let score = document.querySelector('.len span');
    score.innerHTML = snake.len;

	if (snake.death()) {
		console.log("GAME OVER! Your length:", snake.len);
		// background(255, 0, 0);
		// noLoop();
		setup();
		draw();
	}
	//drawing food
	noStroke();
    fill(255, 0, 100);
	rect(food.x, food.y, 1, 1);
}

function keyPressed() {
	// if (key == ' ') snake.grow();
	switch (keyCode) {
		case UP_ARROW:    snake.setDir(0, -1); break;
		case DOWN_ARROW:  snake.setDir(0, 1); break;
		case LEFT_ARROW:  snake.setDir(-1, 0); break;
		case RIGHT_ARROW: snake.setDir(1, 0); break;
	}
}

function foodLocation() {
	let x = floor(random(w));
	let y = floor(random(h));
	food = createVector(x, y);
}