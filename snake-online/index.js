const app  = require('express')();
const http = require('http').Server(app);
const io   = require('socket.io')(http);

const Snake = require('./snake');
const Apple = require('./apple');

const GRID_SIZE   = 40;
const APPLE_COUNT = 10;

let autoId        = 0;
let players       = [];
let apples        = [];

app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

http.listen(3000, () => {
	console.log('listening on *:3000');
});


io.on('connection', (client) => {
	let player;
	let id;

	client.on('auth', (opts, cb) => {
		// Create player
		id = ++autoId;
		player = new Snake({
			id,
			dir: 'right',
			gridSize: GRID_SIZE,
			snakes: players,
			apples,
		}, opts);
		players.push(player);
		// Callback with id for nickname
		cb({ id: autoId });
	});

	client.on('key', (key) => {
		if (player) player.changeDir(key);
	});

	client.on('disconnect', () => {
		let i = 0;
		players.forEach((p) => {
			if (p == player) {
				players.splice(i, 1);   
			}
			i++;
		});
	});
});

// Create apples
for (let i = 0; i < APPLE_COUNT; i++) {
	apples.push(new Apple({
		gridSize: GRID_SIZE,
		snakes: players,
		apples
	}));
}

// Main loop
setInterval(() => {
	players.forEach((p) => {
		p.move();
	});
	io.emit('state', {
		players: players.map((p) => ({
			x: p.x,
			y: p.y ,
			id: p.id,
			nickname: p.nickname,
			points: p.points,
			tail: p.tail
		})),
		apples: apples.map((a) => ({
			x: a.x,
			y: a.y
		}))
	});
}, 100);

