const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin'); 
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
const {CleanWebpackPlugin} = require('clean-webpack-plugin'); 

module.exports = {
  // mode: 'development',
  entry: ["./src/js/main.js"],
  output: {
    filename: "[contenthash].js",
    path: path.resolve(__dirname, 'dist')
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 9000,
    watchContentBase: true,
    progress: true
  },
  plugins: [
    new HtmlWebpackPlugin({template: './src/index.html'}),
    new CleanWebpackPlugin(), 
    new webpack.LoaderOptionsPlugin({
      options: { 
        postcss: [
          autoprefixer()
        ]
      }
    }),
    new MiniCssExtractPlugin({
      filename: '[contenthash].css'
    })
  ],
  
  module: {
    rules: [
      {
        test: /\.jsx$/,
        exclude: /(node_modules)/,
        loader: "babel-loader",
        options: {
          presets:["@babel/preset-env", "@babel/preset-react"],
          sourceMap: true
        }
      },
      {
        test: /\.s[ac]ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {}
          },
          {
            loader: "css-loader",
            options: { importLoaders: 1 } 
          },
          "postcss-loader",
          {
            loader: "sass-loader",
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|svg|jpg)$/,
        loader: "file-loader",
        options: {
          outputPath: 'imgs',
        }
      }
    ]
  }
};